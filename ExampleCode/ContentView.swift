//
//  ContentView.swift
//  ExampleCode
//
//  Created by GayaneA on 4/3/20.
//  Copyright © 2020 ayanemay. All rights reserved.
//


import SwiftUI
import AVKit

struct ContentView: View {
    
    let players:[AVPlayer] =  Array(repeating: AVPlayer(url: Bundle.main.url(forResource: "clickOne", withExtension: "wav")!), count: 5)
    
    //5 palyers with differnet sounds
    let players2: [AVPlayer] =  [
        AVPlayer( url: Bundle.main.url(forResource: "clickOne", withExtension: "wav" )! ),
        AVPlayer( url: Bundle.main.url(forResource: "1", withExtension: "wav" )! ),
        AVPlayer( url: Bundle.main.url(forResource: "2", withExtension: "wav" )! ),
        AVPlayer( url: Bundle.main.url(forResource: "3", withExtension: "wav" )! ),
        AVPlayer( url: Bundle.main.url(forResource: "4", withExtension: "wav" )! )
    ]
    
    
    
    var body: some View {
        
        Text("Hello, World!")
            .onAppear(perform: playMultySounds)
    }
    
    
    
    func playMultySounds(){
        //record the start of the function before the loop is played
        let start = Date()
        
        for player in players2{
            player.play()
        }
        //record when the loop ended
        let end = Date()
        
        /*
         Print how long the entire loop lasted:
         the result is  150000 nanoseconds maximum for the entire loop, the delay between loops is from 15000 to 50000 nanoseconds(0.05 mls)
         */
        
        let difference = Calendar.current.dateComponents([.nanosecond], from: start, to: end)
        let duration = difference.nanosecond!
        print(duration)
    }
    
    
    
    //Second function that synchronises the date the players are played
    
    //Play all players at the exact time - it will be 0.01 seconds after the function starts. Each player will start after the same delay from the start of the function. In the console the date of player playing with miliseconds at the end is printed)
    
    func playMultySounds2(){
        
        let when = DispatchTime.now()
        // let start = Date()
        for i in 0...4{
            DispatchQueue.main.asyncAfter(deadline: when + 0.01) {
                self.players2[i].play()
                
                //print the date of player playing with miliseconds at the end)
                let d = Date()
                let df = DateFormatter()
                df.dateFormat = "y-MM-dd H:m:ss.SSS"
                print( df.string(from: d) )
            }
        }
        
    }
}


//the other option will be to create scheduled timers for each players. But IOS is not so much presize with time, the result will be the same.


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}



